﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes; //posibles ejes de escalado
    public float scaleUnits; //velocidad de escalado

       // Update is called once per frame
    void Update()
    {
        //Acotacion de los valores de escalado al valor unitario [-1, 1]
        axes = CapsuleMovement.ClampVector3(axes);

        //la escala, al contrario que la rotacion y el movimiento, es acumulativa
        //lo que quiere decir que debemos añadir el nuevo valor de la escala, al valor anterior
        transform.localScale += axes * (scaleUnits * Time.deltaTime);
    }
}